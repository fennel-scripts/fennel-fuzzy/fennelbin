#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))

(local lxterm (term:new {:name "lxterm"})
	   ;;{:spawn term.spawn}
	   )

(lambda lxterm.create [{:title ?title :prog ?prog
				  :class ?class :name ?name
				  :embed ?embed
				  :keep ?keep
				  :width ?width :height ?height}]
  (local args ["lxterminal"])
  (local add (partial table.insert args))
  (when ?class	(add (util.set-name ?class)))
  (when ?name	(add (util.set-name ?name)))
  (when ?title	(add (.. "--title '" ?title "'")))
  (when (and ?width ?height)	(add (.. "--geometry=" ?width "X" ?height)))
  ;;(when ?embed	(add (util.embed "-embed" ?embed)))
  (when ?prog	(add (util.set-prog ?prog "--command")))
  (print (tcat args " "))
  (tcat args " "))



lxterm
