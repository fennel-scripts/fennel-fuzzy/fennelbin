#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))

(lambda util.set-prog [prog ?flag]
  (.. (or ?flag "-e") " -- " prog)
  )


(local wezterm (term:new {:name "wezterm"}))
(lambda wezterm.create [{:title ?title :prog ?prog
					:class ?class :name ?name
					:keep ?keep
				    :width ?width :height ?height}]
  
  (local args ["wezterm start"])
  (local add (partial table.insert args))
  (when ?class	(add (util.set-class ?class)))
  ;;(add "--always-new-process ")
  ;;(when ?title	(add (.. "--title " ?title)))
  ;;(when ?width	(add (.. "--width " ?width)))
  ;;(when ?height (add (.. "--height " ?height)))
  (when ?prog	(add (util.set-prog ?prog)))
  (tcat args " "))

wezterm
