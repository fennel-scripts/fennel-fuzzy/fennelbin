#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))
(local glass (require :glass))
(local rc
       (. (glass.bind (.. (or (os.getenv "XDG_CONFIG_HOME") (os.getenv "XDG_CONFIG_DIR") (.. (os.getenv "HOME") "/.config")) "/fennelScripts")
					  [(require :glass.fennel)
					   (require :glass.error)]) :rc))



(local urxvt (term:new {:name "urxvt"})
	   ;;{:spawn term.spawn}
	   )

(lambda urxvt.create [{:title ?title :prog ?prog
				  :class ?class :name ?name
				  :embed ?embed :keep ?keep
				  :width ?width :height ?height}]
  (local args [(or rc.term.urxvt.bin "urxvt")])
  (local add (partial table.insert args))
  (when ?class	(add (util.set-class	?class	"-name")))
  (when ?name	(add (util.set-name		?name	"-name")))
  (when ?title	(add (.. "-title '"		?title	"'")))
  (when (and ?width ?height)	(add (.. "-g " ?width "X" ?height)))
  (when ?embed	(add (util.embed ?embed)))
  (when ?prog	(add (util.set-prog ?prog)))
  (tcat args " "))



urxvt
