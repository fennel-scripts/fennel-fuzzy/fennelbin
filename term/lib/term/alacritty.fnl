#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))
(local alacritty (term:new {:name "alacritty"}))
	   ;;{:spawn term.spawn}

(lambda alacritty.create [{:title ?title :prog ?prog
					  :class ?class :name ?name
					  :embed ?embed :keep ?keep
					  :width ?width :height ?height}]
  (local args ["alacritty"])
  (local add (partial table.insert args))
  ;;(when (or ?class ?role) (add (.. "--class " (or ?class ?role))))
  (when ?class	(add (util.set-class ?class)))
  ;;(when ?class	(add (util.set-class ?class)))
  (when ?title	(add (.. "--title '" ?title "'")))
  (when ?width	(add (tcat ["--option '" "window.dimensions.columns=" ?width "' "])))
  (when ?height	(add (tcat ["--option '" "window.dimensions.lines=" ?height "' "])))
  (when ?keep	(add (.. "--hold")))
  ;;(when ?height (add (.. "--height " ?height)))
  (when ?embed	(add (util.embed ?embed "--embed")))
  (when ?prog	(add  (.. "-e " ?prog)))
  (print (tcat args " "))
  (tcat args " "))



alacritty
