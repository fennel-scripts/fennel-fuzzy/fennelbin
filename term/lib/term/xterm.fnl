#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))

(local xterm (term:new {:name "xterm"})
	   ;;{:spawn term.spawn}
	   )

(lambda xterm.create [{:title ?title :prog ?prog
				  :class ?class :name ?name
				  :embed ?embed :keep ?keep
				  :width ?width :height ?height}]
  (local args ["xterm"])
  (local add (partial table.insert args))
  (when ?class	(add (util.set-class ?class "-class")))
  (when ?name	(add (util.set-name ?name "-name")))
  (when ?title	(add (.. "-title '" ?title "'")))
  (when (and ?width ?height)	(add (.. "-g " ?width "X" ?height)))
  (when ?embed	(add (util.embed ?embed)))
  (when ?prog	(add (.. "-e" ?prog)))
  (tcat args " "))



xterm
