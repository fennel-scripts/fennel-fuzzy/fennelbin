#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))

(local tym (term:new {:name "tym"})
	   ;;{:spawn term.spawn}
	   )

(lambda util.set-prog [prog ?flag]
  (.. (or ?flag "-e") " '" prog "'"))

(lambda tym.create [{:title ?title :prog ?prog
				:class ?class :name ?name
				:keep ?keep
				:width ?width :height ?height}]
  (local args ["tym"])
  (local add (partial table.insert args))
  (when (or ?class ?name)	(add (util.set-class (or ?class ?name) "--role")))
  (when ?title	(add (.. "--title '" ?title "'")))
  (when ?width	(add (.. "--width=" ?width)))
  (when ?height (add (.. "--height=" ?height)))
  (when ?prog	(add (util.set-prog ?prog )))
  (tcat args " "))



tym
