#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))

(local st (term:new {:name "st"}))
(lambda st.create [{:title ?title :prog ?prog
			   :class ?class :name ?name
			   :embed ?embed :keep ?keep
			   :width ?width :height ?height}]
  (local args ["st"])
  (print "embed" ?embed)
  (local add (partial table.insert args))
  (when ?class					(add (util.set-class ?class "-c ")))
  (when ?name					(add (util.set-name ?name "-n ")))
  (when ?title					(add (.. "-t '" ?title "'")))
  (when (and ?width ?height)	(add (.. "-g " ?width "X" ?height)))
  (when ?embed					(add (util.embed ?embed "-w")))
  (when ?prog					(add (.. "-e " ?prog)))
  (tcat args " "))

st
