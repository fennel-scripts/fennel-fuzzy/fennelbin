#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))

(local xfce-term (term:new {:name "xfce4-terminal"})
	   ;;{:spawn term.spawn}
	   )

(lambda xfce-term.create
  [{:title ?title :prog ?prog
	:class ?class :name ?name
	:embed ?embed :keep ?keep
	:width ?width :height ?height}]
  (local args ["xfce4-terminal"])
  (local add (partial table.insert args))
  (when ?class	(add (util.set-class ?class)))
  (when ?name	(add (util.set-name ?name)))
  (when ?title	(add (.. "--title '" ?title "'")))
  (when (and ?width ?height)	(add (.. "--geometry " ?width "X" ?height)))
  ;;(when ?embed	(add (util.embed ?embed)))
  (when ?keep	(add "--hold"))
  (when ?prog	(add (util.set-prog ?prog)))
  (tcat args " "))



xfce-term
