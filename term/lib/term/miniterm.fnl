#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)
(local term (require :lib.term))
(local util (require :lib.util))

(local miniterm (term:new {:name "miniterm"})
	   ;;{:spawn term.spawn}
	   )

(lambda set-prog [prog ?flag]
  (.. (or ?flag "-e") " '" prog "'")
  )

(lambda miniterm.create [{:title ?title :prog ?prog
					 :class ?class :name ?name
					 :keep ?keep
					 :width ?width :height ?height}]
  (print "" (set-prog (or ?prog "none")))
  (local args ["miniterm"])
  (local add (partial table.insert args))
  (when ?class	(add (util.set-class ?class)))
  (when ?name	(add (util.set-name ?name)))
  (when ?title	(add (.. "--title '" ?title "'")))
  ;;(when ?width	(add (.. "--columns=" ?width)))
  ;;(when ?height (add (.. "--rows=" ?height)))
  (when ?keep	(add (.. "--keep")))
  (when (and ?prog (not= ?prog "") (not= ?prog " "))	(add (set-prog ?prog) ))
  (tcat args " "))



miniterm
