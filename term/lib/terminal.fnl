#!/usr/bin/env fennel
(local tym			(require :lib.term.tym))
(local miniterm		(require :lib.term.miniterm))
(local st			(require :lib.term.st))
(local wezterm		(require :lib.term.wezterm))
(local alacritty	(require :lib.term.alacritty))
(local xterm		(require :lib.term.xterm))
(local urxvt		(require :lib.term.urxvt))
(local xfce-term	(require :lib.term.xfce-term))
(local lxterm		(require :lib.term.lxterm))



(local glass (require :glass))
;;(local conf (require :conf))
(local fennel (require :fennel))


(local conf (. (glass.bind
				(.. (or (os.getenv "XDG_CONFIG_HOME") (os.getenv "XDG_CONFIG_DIR") (.. (os.getenv "HOME") "/.config") ) "/fennelScripts")
				[(require :glass.fennel)
				 (require :glass.error)]) :rc))

;;(os.exit)

(local terminals {: st
				  : tym
				  : wezterm
				  : alacritty
				  : xterm
				  : lxterm
				  : xfce-term
				  : urxvt
				  : miniterm
				  })
(local rc
	   (or conf.term
		   {:minimal :st
			:mini :urxvt
			:default :xterm}))


(collect [k v (pairs rc) &into terminals]
  k (. terminals v))


;;(print (fennel.view terminals ))

terminals
