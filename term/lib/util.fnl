#!/usr/bin/env fennel

(local util {})
(lambda util.set-class [class ?flag]
  (let [flag (or ?flag "--class")]
	(case
	 (type class)
	 :table (.. flag " '" (or (. class 1) class.class) "'") ;; why tho
	 :string (.. flag " '" class "'"))))

(lambda util.set-name [name ?flag]
  (util.set-class name (or ?flag "--name")))

(lambda util.set-prog [prog ?flag]
  (print (.. "util.set-prog" prog ":" ?flag))
  (.. (or ?flag "-e") prog)
  )

(lambda util.embed [wid ?flag]
  (.. (or ?flag "-into") " " wid))


util
