#!/usr/bin/env fennel
(local fennel (require :fennel))
(local tcat table.concat)
(local view fennel.view)

(local term {})

(lambda term.create [{:title ?title :prog ?prog
				 :class ?class :name ?name
				 :keep ?keep
				 :width ?width :height ?height}]
  "template create function.
mostly here so that calling `(: (term:new) :spawn {<props>})`
won`t error out compleately

overloads must return a string
"
  {:title ?title :prog ?prog
   :class ?class :name ?name
   :keep (or ?keep false)
   :width ?width :height ?height}
  )

(lambda term.new [this {:name ?name :create ?create}]
  (let [trm {:name (or ?name :term) :create (or ?create term.create)}]
	(setmetatable trm this)
	(set this.__index this)
	;;(print (view [this]))
	trm))
  
 

(lambda term.open [t 
			  {:title ?title :prog ?prog
			   :class ?class :name ?name
			   :keep ?keep
			   :width ?width :height ?height}]
		(t.create
		 {:title ?title :prog ?prog
		  :class ?class	:name ?name
		  :keep ?keep
		  :width ?width :height ?height})
	)

(lambda term.spawn* [t {:title ?title :prog ?prog
				  :class ?class :name ?name
				  :width ?width :height ?height}]
  (let [trm
		(t.create
		 {:title ?title :prog ?prog
		  :class ?class 
		  :name	 ?name
		  :width ?width :height ?height})]
	(os.execute trm)))

(lambda term.spawn [t arguments]
  (os.execute (t:open arguments))
  )








term
