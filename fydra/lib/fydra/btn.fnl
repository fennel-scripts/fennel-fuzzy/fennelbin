#!/usr/bin/env fennel


(local btn {})

(lambda btn.new [o]
  (match (type o)
	:string {:char o :key "char"}
	:number {:char (tostring o) :key "char"}
	:table o
	
	)
  )

btn
