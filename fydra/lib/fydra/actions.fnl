#!/usr/bin/env fennel
(local terminal (require :terminal))
(local actions {})
;;(fn pp [...] "pretty-printing function" (let [fennel (require :fennel)] (print (fennel.view ...)))) 
(local actions {})


(lambda actions.term [prog]
  (let [term terminal.minimal]
	(term:spawn {:class "float" :prog prog})
	))

(lambda devour [...]
  (os.execute (.. "devour " ...)))
(lambda actions.term-replace [prog]
  (let [term terminal.miniterm]
	(devour (term:open {:class "float" :prog prog}))
	))

(lambda actions.exec [prog]
  (match (type prog)
	:string	(os.execute prog)
	:table	(os.execute (table.concat prog " "))))

(lambda actions.execbg [prog]
  (match (type prog)
	:string	(actions.exec (.. prog "&"))
	:table	(do (table.insert prog "&") (actions.exec prog))))


;;(actions.term "batman emacs")


;;{: term : term-replace : exec : execbg}
actions
