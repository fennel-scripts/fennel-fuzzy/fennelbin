#!/usr/bin/env fennel

(local entry {})

(lambda entry.new
  [{: key
	:action ?action
	:args ?args
	:desc ?desc :description ?description
	:clear ?clear
	:exit ?exit
	:props ?props}]
  {: key
   :action (or ?action :noop)
   :args (or ?args "")
   :desc (or ?desc ?description "tbd")
   :clear (or ?clear true)
   :exit (or ?exit false)
   :props (or ?props {})})



entry

