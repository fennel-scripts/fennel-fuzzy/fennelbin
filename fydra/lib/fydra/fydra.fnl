#!/usr/bin/env fennel
;; (local templates (require :fydra.templates))
(local fydra {})
(local entry (require :fydra.entry))
(local fort (require :fort))
(local btn (require :fydra.btn))


(local tcat table.concat)

(fn pp [...] "pretty-printing function" (let [fennel (require :fennel)] (print (fennel.view ...))))

(lambda fydra.build [this]
  (set this.fort (fort.new))

(this.fort:set_border_style fort.DOUBLE_STYLE)

;; configure head
(-> this.fort
   (: :set_cell_prop 1 fort.ANY_COLUMN fort.CPROP_ROW_TYPE fort.ROW_HEADER)
   (: :set_cell_prop 1 fort.ANY_COLUMN fort.CPROP_TEXT_ALIGN fort.ALIGNED_CENTER)
   (: :set_cell_span 1 1 (* (or (. this.props :width) 2) 2))
   (: :write_ln this.header)
   )


  
(each [key entris (ipairs this.tbl)]
  (each [k entry (ipairs entris)]
	(var button entry.key)
	(when (= (type button) :table)
	  (if (= button.key "f")
		  (set button (.. button.key button.char))
		  (set button button.char)
		  )
	  (match button
		{:key "f" : char} (do (print "fkey" char) (os.execute "sleep 2"))
		)
	  
	  )
	(-> this.fort
	   ;;(fort.CUR_ROW 1 fort.CPROP_CONT_FG_COLOR (or entry.key-colour))
	   (: :set_cell_prop fort.CUR_ROW fort.CUR_COLUMN fort.CPROP_CONT_FG_COLOR (or entry.key-colour fort.COLOR_BLUE))
	   (: :write button)
	   (: :write entry.desc)
	   )
	  )
  (this.fort:ln)))


(lambda fydra.draw [this]
  (print this.fort)
  )

(lambda fydra.add-entries! [this entries]
  (each [key value (ipairs entries)]
	;;(set v.key (btn.new v.key))
	(each [k v (ipairs value)]
	  (set v.props (or v.props {})) (set v.props.btn (btn.new v.key)) )
	(collect		[k v (ipairs value) &into this.entrys]	v.key	(entry.new v)))
  ;;(icollect		[k v (ipairs entries) &into this.tbl]				(entry.new v))
  (icollect [k v (ipairs entries) &into this.tbl] v)
  )

(lambda fydra.new
  [this [name ?props ?header] & entrys]
  (let [entries entrys
		obj {: name :header (or ?header name) :fort (fort.new) :entrys [] :tbl [] :props (or ?props {})}]
	
	;;(each	[k v (ipairs entries)] (pp v) )
	;;(collect	[k v (ipairs entries) &into obj.entrys] v.key (entry.new v))
	;;(icollect	[k v (ipairs entries) &into obj.tbl] (entry.new v))
	
	(fydra.add-entries! obj entries)
    (setmetatable obj this)
 	(set this.__index this)
 	obj))






fydra
