#!/usr/bin/env fennel
;;; # simple stack library for fennel
(local fennel (require :fennel))
(local view fennel.view)
(local frame (require
			  ;;:lib.frame
			  :helheim.frame
			  ))
(local plates {:stack []})
(lambda plates.push [this value]
  "push value to the top of the stack"
  (table.insert this.stack value))

(lambda plates.pop [this]
  "get the value at the top of the stack"
  (table.remove this.stack))

(lambda plates.clear [this]
  "throw the stack in the trash"
  (set this.stack []))

(set plates.throw	plates.clear)
(set plates.trash	plates.clear)
(set plates.bin		plates.clear)

(lambda plates.view [this]
  ;;(print (frame.center 20 this.name))
  ;; (for [i (# this.stack) 0 -1]
  ;; 	(print i (fennel.view (?. this.stack i))))
  ;;(each [key value (pairs this.stack)](print key (fennel.view value)))
  (print (fennel.view {:name this.name :stack this.stack}))
  this)

(lambda plates.peek [this] (. this.stack (# this.stack)))

(lambda plates.restack [this]
  (let [newstack []]
	(each [key value (ipairs this.stack)]
	  (table.insert newstack 1 value))
	(set this.stack newstack)))

(lambda plates.new [this {:name ?name :stack ?stack}]
  (let [plate
		{:name	(or ?name "base")
		 :stack	(or ?stack [])}]
    (setmetatable plate this)
	;;(print (view [this plate]))
    (set this.__index this)
    plate))


plates
