#!/usr/bin/env fennel
(local fennel (require :fennel))
(local utils (require :lambda.utils))
(local fil (require :fil))

(local plates (require :helheim.plates))

(local view fennel.view)
(local tcat table.concat)
(local tunpack table.unpack)
(local tinsert table.insert)

(local log {})
(lambda log.print [prog] (print prog.wmem.name (view prog.mem.stack) (view prog.stack.stack)))





(local hel {})


(lambda hel.add [stack args]
  (faccumulate [n 0 i 1 args]
    (+ n (or (stack:pop) 0))))

(lambda hel.subtract [stack args]
  (faccumulate [n 0 i 1 args]
	(- n (stack:pop))))

(lambda hel.divide [stack args]
  (math.floor
   (faccumulate [n 0 i 1 args]
	 (/ n (stack:pop)))))

(lambda hel.multiply [stack args]
  (math.floor
   (faccumulate [n 0 i 1 args]
	 (* n (stack:pop)))))

(lambda hel.compare [stack] (= (stack:pop) (stack:pop)))

(lambda hel.noop [...] ...)
(lambda hel.noop* [...]
  (when ...
   (let [args [...]]
	(print (view (or args ""))))))


(lambda hel.parse [prog line ?lino]
  (let [wmem prog.wmem
		mem prog.mem
		stack prog.stack
		noop hel.noop]
	
	(local command (line:pop))
	(match command
	  ;;comments
	  :#	(noop line.stack)
	  ";"	(noop line.stack)
	  ";;"	(noop line.stack)
	  ";;;" (noop line.stack)
	  )
	
	(match command
	  ;;stack-utils
	  :>	(wmem:pop)
	  :<	(wmem:push (line:pop))
	  :&<	(wmem:push (stack:pop))
	  :%<	(wmem:push (mem:pop))
	  :><	(wmem:restack)
	  :>>	(wmem:view)
	  :%	(set prog.wmem prog.stack)
	  :&	(set prog.wmem prog.mem)
	  ;; printing to screen
	  :>!	(print ">>" (line:pop))
	  :!>	(print ">>" (line:pop))
	  :>!!	(do (for [i 1 (line:pop) 1]
				  (io.write (.. " " (wmem:pop)))) (print ""))
	  :>!*	(print (view stack))
	  ;;math
	  :-	(wmem:push (hel.subtract	wmem	(line:pop)))
	  :*	(wmem:push (hel.multiply	wmem	(line:pop)))
	  :/	(wmem:push (hel.divide		wmem	(line:pop)))
	  :+	(wmem:push (hel.add			wmem	(line:pop)))
	  ;;logic
	  :?	(if
			 (hel.compare wmem)
			 (wmem:push (line:pop))
			 (do (line:pop)
				 ;;(if line)
				 (wmem:push (or (line:pop) 0))
				 ))
	  :<!	(wmem:push (io.read))
	  
	  ;;tbd not yet implemented but "reserved" keywords
	  :->	(noop line.stack)
	  :-->	(noop line.stack)
	  :=>	(noop line.stack)
	  :==>	(noop line.stack)
	  :<-	(noop line.stack)
	  :<--	(noop line.stack)
	  :<=	(noop line.stack)
	  :<==	(noop line.stack)
	  ;; ending the program
	  :quit	(os.exit)
	  ":q"	(os.exit)
	  :exit	(os.exit)
	  )))

(lambda hel.new [name tape]
  (local mem (plates:new {:name "mem"}))
  (local stack (plates:new {:name "stack"}))
  {:eval hel.eval
   :read-tape hel.read-tape
   :parse hel.parse
   : tape
   :mem mem
   :stack stack
   :wmem stack})

(lambda hel.read-tape [prog]
  (log.print prog)
  (each [key value (ipairs prog.tape)]
	(let [line (plates:new {:name (.. "[" key "]") :stack value})]
	  ;;(print line.name (view line.stack))
	  ;;(print line.name (view line.stack))
	  (prog:eval line)
	  )))

(lambda hel.eval [prog line ?key]
  (print (view line.stack))
  (prog:parse line ?key)
  (log.print prog)
)


hel
