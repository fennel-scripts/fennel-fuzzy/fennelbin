#!/usr/bin/env fennel
(local rep string.rep)
(local floor math.floor)
(local ceil math.ceil)
(local tcat table.concat)

(fn padd [op len text] (rep "=" (op (- (/ len 2) (/ (length text) 2)))))

(lambda center [?len text]
  (local len (or (tonumber ?len) 80))
  (tcat
   [(padd floor len text)
	text
	(padd ceil len text)]))

(lambda frame [text ?len]
  (local len (or (tonumber ?len) 80))
  (tcat [(rep "=" len)
		 (center text)
		 (rep "=" len)]))

;;(frame :libraries 30)
;;(frame :elven 30)

;;(frame (or (. arg 1) "***") (. arg 2))
{: frame : center}
