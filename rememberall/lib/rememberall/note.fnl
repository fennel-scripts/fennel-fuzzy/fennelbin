#!/usr/bin/env fennel

(local tcat table.concat)
(local fennel (require :fennel))
(local view fennel.view)
(fn tprint [...] " table-printing function " (print (tcat ...)))
(fn pp [...] "pretty-printing function" (print (view ...)))
(local note {})


;;#TODO: write a MERGE function that uses collect to merge(overlay) two tables

(lambda note.new [this template ?meta]

  (local defaults {:title "" :author "" :tags [] :refs [] :content []})
  (collect [key value (pairs template) &into defaults] key value)
  
  (when ?meta (collect [key value (pairs ?meta) &into defaults] key value))
  (let [obj defaults]
	(setmetatable obj this)
	(set this.__index this)
	obj))


note
