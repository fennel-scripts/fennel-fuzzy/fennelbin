#!/usr/bin/env fennel

(local cmd {})
(local tcat table.concat)
(local stow-actions {:stow "--stow" :unstow "--delete" :restow "--restow"})

(λ cmd.exec [command ?sep]
  "execute:
execute system command defined in the first argument(string or squential table) and
an optional separator as the second argument
the default separator is ' ' (a single space) so you dont have to pad the strings."
  (let [sep (or ?sep " ")]
	(match (type command)
	  :table	(os.execute (tcat command sep))
	  :string	(os.execute command))))

(λ cmd.tprint [command ?sep]
  "execute:
execute system command defined in the first argument(string or squential table) and
an optional separator as the second argument
the default separator is ' ' (a single space) so you dont have to pad the strings."
  (let [sep (or ?sep " ")]
	(match (type command)
	  :table	(print (tcat command sep))
	  :string	(print command))))

(λ cmd.install [{: src : tgt :sudo ?sudo}]
  "run the install command with given arguments

includes option to run with sudo
"
	(print src "==>" tgt)
	(cmd.exec [(if ?sudo "sudo " "") "install" "--compare" src tgt])
	)

(λ cmd.stow [{: dir :tgt ?tgt :target ?target :action ?action : pkgs}]
  "wrapper, peform stow action on the given arguments"
  (λ stow [{: dir : tgt :action ?action : pkgs}]
	(var packages pkgs)
	(when (= (type packages) :string)		;make sure the packages(pkgs) is a list/table
	  (set packages [packages]))		;rather than a string, later undone by table.concat(tcat)
	(cmd.exec ["stow" "-v" "-d" dir "-t" tgt (. stow-actions (or ?action "stow")) (tcat packages " ") ]))
  (stow {: dir :tgt (or ?tgt ?target) :action ?action : pkgs}))

(λ cmd.mkdir [{:sudo ?sudo :dirs dirs}]
  "make directory

includes option to run with sudo
"
  ;;(print "****use mkdir* for backwards compatability****")
  ;;(table.insert args 1 "mkdir -pv ")
  (cmd.exec [(if ?sudo "sudo" "") "mkdir" "--parents" "--verbose" (if (= (type dirs) "table") (tcat dirs) dirs)])
  )
  

(fn cmd.mkdir* [args]
  (table.insert args 1 "mkdir -pv ")
  (cmd.exec args ""))


cmd
