#!/usr/bin/env fennel

;;(local fennel (require :fennel))
;;(set package.path (.. package.path ";" confpath "//?.fnl"))
(let [home (os.getenv "HOME")
	  fennel (require :fennel)
	  sysconfig "/etc/bots/"
	  config
  	(or	(os.getenv "XDG_CONFIG_DIR")
		(os.getenv "XDG_CONFIG_HOME")
		(.. (os.getenv "HOME") "/.config"))]
  (set fennel.path
	   (..
		config "/bots/" "rc.fnl" ";"
		config "/fennelScripts/" "?.fnl" ";" ;;"bots/rc.fnl" ";"
		sysconfig "systemrc.fnl" ";"
		fennel.path)))
;;(print "done")
{}

