#!/usr/bin/env fennel
(local cmds (require :wordmatch.cmds))
(local dict {})

(lambda dict.matcher [arguments]

(match arguments
  ["turn" "the" area "lights" state]		(cmds.set area state)
  ["turn" state "the" area "lights"]		(cmds.set area state)
  ["turn" area "lights" state]				(cmds.set area state)
  ["set" "the" area "lights" "to" state]	(cmds.set area state)
  ;;toggle
  ["toggle" area "lights"]					(cmds.toggle area)
  ["toggle" "the" area "lights"]			(cmds.toggle area)
  ["toggle" "the" "lights" "in" area]		(cmds.toggle area)
  ["toggle" "the" "lights" "in" "the" area]	(cmds.toggle area)
  
  )
)



dict
