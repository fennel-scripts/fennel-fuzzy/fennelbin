#!/usr/bin/env fennel
(local areas (require :wordmatch.areas))
(local cmds {})
(lambda cmds.set		[area state]	{: state :device (. areas area)})
(lambda cmds.toggle	[area]			{:state "toggle" :device (. areas area)})

cmds
