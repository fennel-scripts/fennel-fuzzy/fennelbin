#!/usr/bin/env fennel

(local tcat table.concat)
(local strf string.format)
(local gum {:config {}
			:debug false
			})

;;(set gum.debug true)
(fn debug [...]
  (when gum.debug
	(print "debug" ...)))


(lambda capture [cmd]
  ;;(print cmd)
  (local f (assert (io.popen cmd :r)))
  (var s (assert (f:read :*a)))
  (local x [(f:close)])
  (local raw s)
  (set s (string.gsub s "^%s+" ""))
  (set s (string.gsub s "%s+$" ""))
  (set s (string.gsub s "[\n\r]+" " "))
  ;;(each [key value (ipairs x)]	(print " " key value))
  {:out s
   :raw raw
   :bool (. x 3)})




(lambda capt [cmd]
  {:out cmd :raw cmd :bool false})


(lambda gum.capture [cmd]
  (local ret (capture cmd))
  (debug (. (capt cmd) :out))
  (var captured ret.out)
  (when (= ret.out (or "" nil))
	(set captured ret.bool))
  captured)


(fn gum.base [{: action : act : arguments : args}]
  (λ base [{: action : args}]
    (gum.capture (tcat [:gum action (tcat args " ")] " ")))
  (base {:action (or action act) :args (or arguments args)}))

 


(fn gum.choose [options]
  (local opts [])
  (each [key value (ipairs options)]
	(table.insert opts (.. "\"" value "\"")))
  (gum.base {:action "choose" :args options}))


(fn gum.input
  [{: placeholder
	: val
	: prompt
	: header
	: password : passwd : pass
	: char-limit : max-len}]
  (local password (or password passwd pass))
  (local max-length (tonumber (or max-len char-limit)))
  (local args [])
  (when placeholder (table.insert args (tcat ["--placeholder" (strf "%q" placeholder)] " ")))
  (when val (table.insert args (tcat ["--value" (strf "%q" val)] " ")))
  (when header (table.insert args (tcat ["--header" (strf "%q" header)] " ")))
  (when prompt (table.insert args (tcat ["--prompt" (strf "%q" prompt)] " ")))
  (when password (table.insert args "--password"))
  (gum.base {:act "input" : args}))


(fn gum.file [args]
  "pick file from folder"
  (lambda file [{:all ?all :path ?path}]
	(local arguments [])
	(if ?all (table.insert arguments "--all "))
	(if ?path (table.insert arguments  ?path))
	(gum.base {:action "file" : arguments}))
  (file (or args {})))

(fn gum.confirm [args]
  (lambda confirm [{: affirmative : negative : prompt}]
	(let [args
		  [(when affirmative (.. " --affirmative=" affirmative))
		   (when negative (.. " --negative=" negative))
		   " " (or (strf "%q" prompt) "")]]
	  (gum.base {:action "confirm" :args args})))
  (let [arguments
		{:affirmative (or args.affirmative args.affirm args.yes args.true "Yes")
		 :negative (or args.negative args.neg args.no args.false "No")
		 :prompt (or args.prompt args.message args.msg args.question "Are you sure?")
		 }]
	(confirm arguments)))



(lambda gum.spinner
  [{:title ?title
	:spinner ?spinner
	:align ?align		:show-output ?show-out
	:timeout ?timeout	:command command}]
  "
:required
command
:optional
title spinner align show-output timeout
"
  (local args [])
  (when ?title (table.insert args (tcat ["--title" (strf " %q " ?title )])))
  (when ?spinner (table.insert args (tcat ["--spinner" (strf " %q " ?spinner)])))
  (when ?align
	;;(when (or (= ?align :left (= ?align :right)))
	  (table.insert args (tcat ["--align" (strf " %q " ?align)])));;)
  (when ?show-out (table.insert args (tcat ["--show-output" (strf " %q " ?timeout )])))
  (when ?timeout (table.insert args (tcat ["--timeout" (strf " %q " ?timeout )])))
  (when command (table.insert args (tcat ["--" (strf " %s " command)])))
  (gum.base {:act "spin" : args})
  )


(set gum.spinners {})
(set gum.spinners.dot		:dot)
(set gum.spinners.globe		:globe)
(set gum.spinners.hamburger	:hamburger)
(set gum.spinners.jump		:jump)
(set gum.spinners.line		:line)
(set gum.spinners.meter		:meter)
(set gum.spinners.minidot	:minidot)
(set gum.spinners.monkey	:monkey)
(set gum.spinners.moon		:moon)
(set gum.spinners.points	:points)
(set gum.spinners.pulse		:pulse)
	  


;;(io.read "")


gum
