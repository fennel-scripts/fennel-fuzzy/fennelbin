#!/usr/bin/env fennel


(local utils (require :lib.scarab.utils))
(local tcat utils.tcat)
(local pp utils.pp)
(local kv utils.kv)
(local kvlist utils.kvlist)
(local tbl (require :lib.scarab.tbl))

(local debug (require :lib.scarab.dbg))
(local loglvl (require :lib.scarab.enums.loglvl))
(local dbg (debug.new {:level loglvl.normal}))



(local tag {})

;;(lambda tag* [{: tag : content :props ?props :notag ?notag}]	(if ?notag [tag] [(.. "<" tag (kvlist properties) ">") content (.. "</" tag ">")]		))


(lambda tag.htmlize [tag]
  ;;(pp ["x" props])
  ;;(pp ["y" (kvlist props)])
  
  (if
   tag.notag
	[tag.tag]
   tag.void
	[(.. "<" tag.tag (kvlist tag.props) ">")]
   ;;else
	[(.. "<" tag.tag
		 (kvlist tag.props)
		 ">")
	 tag.content
	 (.. "</" tag.tag ">")] ))

(lambda tag.new [{: tag : content :props ?props :notag ?notag :void ?void}]
  ;; (local props {})
  (local props (or ?props []))
  ;; (when ?props ;;(print "props") (pp ?props)
  ;; 	(each [i v (ipairs ?props)] (table.insert props v )))
  ;;(pp ["a" props ?props])
  ;; (when ?extra-props ;;(print "extra-props") (pp ?extra-props)
  ;; 	(each [i v (ipairs ?extra-props)] (table.insert props v)))
  ;;(tbl.merge props ?props)
  
  ;;(print "***************************************************************")
  ;;(pp ["b" props ?extra-props])
  
  (local newtag {: tag : content :props ?props :notag ?notag :void ?void})
  ;;(fn stringer [])
  (setmetatable newtag {:__name (.. "name: " tag)
						:__istag true
						;;:__call call
						}))



;;(setmetatable tag {:__call new})
tag
;;tag.new
