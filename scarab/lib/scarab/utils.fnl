#!/usr/bin/env fennel

(local utils {})

(local pair (require :lib.scarab.pair))
(local tcat table.concat)
(set utils.tcat tcat)

(fn utils.pp [...] (let [fennel (require :fennel)] (print (fennel.view ...))))

(λ utils.++ [x] (+ x 1))
(λ utils.-- [x] (- x 1))

;;(lambda utils.kvlist [?props] (if ?props (tcat (icollect [_ kv (ipairs ?props)] (pair kv))) ""))
(lambda utils.kvlist [?props]
  (if ?props
	;;(utils.pp ["Z" ?props])
	;; (each [key value (ipairs ?props)]
	;;   (utils.pp ["a" key "b" value])
	;;   (each [k v (pairs value)]
	;; 	(string.format " %s=%q " k v)
	;; 	)
	;;   )
	  (do
		(local props ?props)
		(local properties [""])
		(icollect [key value (pairs props) &into properties]
		  (tcat [key "=\"" value "\""]))
		 (tcat properties " "))
	  ""
	 ))

(λ utils.headers []
  (print "Status: 200 OK")
  (print "Content-type: text/html")
  (print "\n")
)



utils
