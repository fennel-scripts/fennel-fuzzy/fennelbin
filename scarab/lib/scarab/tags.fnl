#!/usr/bin/env fennel
(local utils (require :lib.scarab.utils))
(local tag (require :lib.scarab.tag))


(local tcat utils.tcat)
(local pp utils.pp)
(local ++ utils.++)
(local -- utils.--)
(local kv utils.kv)
(local kvlist utils.kvlist)

(local tags {})



;;; core and meta tags
(lambda tags.doctype [] (tag.new {:notag true :tag "<!DOCTYPE html>" :content []}))
(lambda tags.html [content] (tag.new {:tag "html" : content}))
(lambda tags.head [content] (tag.new {:tag "head" : content}))
(lambda tags.title [title]  (tag.new {:tag "title" :content [title]}))
(lambda tags.meta [{: props}] (tag.new {:void true :tag "meta" :content [] : props }))
(lambda tags.body [{: content :props ?props}] (tag.new {:tag "body" : content :props ?props}))


;;; h<n>tags

(lambda tags.hgroup [{: lvl : content :props ?props}]
  (tag.new {:tag "hgroup" : content :props ?props}))

(λ tags.htag [{: lvl : text :props ?props}]
  (tag.new {:tag (.. "h" lvl) :content text :props ?props}))

(lambda tags.h [{: lvl : text :props ?props}]
  (tags.htag {: lvl :text (.. (string.rep "#" lvl) " " text) :props ?props}))

;;;; autogen h<n> tags and h<n>tag tags
(for [i 1 6]
  (tset tags (.. :h (tostring i))
   (lambda [{: text :props ?props}]
     {:fnl/docstring (.. "shorthand h" i " tag.new with nice styling.")}
     (tags.h {:lvl i : text :props ?props})))
  (tset tags (.. :h (tostring i) :tag)
   (lambda [{: text :props ?props}]
     {:fnl/docstring (.. "shorthand h" i "tag.new tag.")}
     (tags.htag {:lvl i : text :props ?props}))))

;;; common tags


;;;; semantic tags

(lambda tags.article [{: content :props ?props}]
  (tag.new {:tag "article" :content content :props ?props}))
(lambda tags.aside [{: content :props ?props}]
  (tag.new {:tag "aside" :content content :props ?props}))
(lambda tags.details [{: content :props ?props}]
  (tag.new {:tag "details" :content content :props ?props}))
(lambda tags.figcaption [{: content :props ?props}]
  (tag.new {:tag "figcaption" :content content :props ?props}))
(lambda tags.figure [{: content :props ?props}]
  (tag.new {:tag "figure" :content content :props ?props}))

(lambda tags.header [{: content :props ?props}]
  (tag.new {:tag "header" :content content :props ?props}))
(lambda tags.nav [{: content :props ?props}]
  (tag.new {:tag "nav" :content content :props ?props}))
(lambda tags.main [{: content :props ?props}]
  (tag.new {:tag "main" :content content :props ?props}))
(lambda tags.section [{: content :props ?props}]
  (tag.new {:tag "section" :content content :props ?props}))
(lambda tags.mark [{: content :props ?props}]
  (tag.new {:tag "mark" :content content :props ?props}))
(lambda tags.summary [{: content :props ?props}]
  (tag.new {:tag "summary" :content content :props ?props}))
(lambda tags.time [{: content :props ?props}]
  (tag.new {:tag "time" :content content :props ?props}))
(lambda tags.footer [{: content :props ?props}]
  (tag.new {:tag "footer" :content content :props ?props}))


;;;; style tags


(lambda tags.hr [{:content ?content :props ?props }]
  "horizontal rule
adds a horizontal line
"
  (tag.new {:void true :tag "hr" :content [] :props ?props})
  )

(lambda tags.br [{:props ?props }]
  "BReak line
Insert single line break in some text
"
  (tag.new {:void true :tag "br" :content [] :props ?props})
  )
;;(lambda tags.meta [{: props}] (tag.new {:void true :tag "meta" :content [] :props ?props }))


;;;; div
(lambda tags.div [{: children :props ?props}]
  (tag.new {:tag "div" :content children :props ?props}))

;;;; p,ptag
(lambda tags.p [{: text :props ?props}]
  (tag.new {:tag "p" :content text :props ?props }))
(set tags.ptag tags.p)
(set tags.paragraph tags.p)


;;;; list
(local list {})
(λ list.item [{: content :props ?props}]
	(tag.new {:tag "li" : content :props ?props }))
(set list.li list.item)
(lambda list.__new [{: tagname : content :props ?props }]
	(tag.new {:tag tagname : content :props ?props}))
(lambda list.ordered [{:content ?content :items ?items :props ?props}] (list.__new {:tagname "ol" :content (or ?items ?content) :props ?props}))
(set list.ol list.ordered)
(lambda list.unordered [{:content ?content :items ?items :props ?props}] (list.__new {:tagname "ul" :content (or ?items ?content) :props ?props}))
(set list.ul list.unordered)
(set tags.list list)



;;; extern
;;tags with external content, links images etc
;;;; link

(lambda tags.a [{: href : content :props ?props}]
  ;;(local properties (or ?props {}))
  ;;(tset properties :href href)
  (local properties {: href})
  (when ?props (collect [k v (pairs ?props) &into properties] k v))
  (tag.new {:tag "a" : content :props properties})
  )

(lambda tags.link [{: href : rel :type ?type :props ?props}]
  ;;(local properties (or ?props {}))
  ;;(tset properties :href href)
  (local properties {: href : rel :type ?type})
  (when ?props (collect [k v (pairs ?props) &into properties] k v))
  (tag.new {:void true :tag "link" :content [""] :props properties}))

(lambda tags.link-css [href ?props]
  (tags.link {: href :rel "stylesheet" :props ?props}))

(lambda tags.link-feed [href ?props]
  (tags.link {: href :rel "alternate" :type "application/rss+xml" :props ?props}))

;;;; img,image
(lambda tags.img* [url alt width height]
  "creates the equivalent html-tag. this is an image"
  (.. "\t\t" "<img src=\"" url "\" alt=\"" alt "\" width=\"" width
	  "\" height=\"" height "\">" "\n")
  (tag.new {:void true :tag "img" :content "" :props {:src url : alt : width : height}}))

(lambda tags.img [{: url : alt } {:width width :height height} {:id ?id :class ?class}]
  (tag.new {:void true :tag "img" :content "" :props {:src url : alt : width : height :id ?id :class ?class}}))

(lambda tags.image [image]
  (tags.img {:url image.url :alt image.alt} {:width image.width :height image.height} {:id image.id :class image.class}))

;;; icon-stuff
(λ tags.link-icons []
  (tags.link-css "https://fonts.googleapis.com/icon?family=Material+Icons"))

(λ tags.icon [{: icon :class ?class :id ?id}]
  (local class [])
  (table.insert class "material-icons")
  (when ?class (icollect [_ c (ipairs ?class) &into class] c))
  (tag.new {:tag "span" :content icon :props {:class (tcat class " ") :id ?id}})
  )

;;; other functions

(lambda tags.detangle [tbl ?i]
  (let [i (or ?i 0)]
	(each [k val (pairs tbl)]
	  ;;(pp {:abc val})
	  (match (type val)
		:table ;;(do (pp [:X val ]) (pp [:Y (getmetatable val)])
		(if val.tag
			(tags.detangle (tag.htmlize val) (++ i))
			(tags.detangle val (++ i)));;)
		:string
		(if (> i 1)
			(print (.. (string.rep "  " i) val))
			(print val))))))

tags
