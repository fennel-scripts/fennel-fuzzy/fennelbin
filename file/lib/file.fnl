#!/usr/bin/env fennel
(local f {})
(local tcat table.concat)


(λ read! [this]
  (let [lines []]
	(with-open [file-in (io.open this.name)]
	  (each [line (file-in:lines)]
		(table.insert lines line)))
	(set this.lines lines)))

(λ write! [this]
  (with-open [file-out (io.open this.name :w)]
	(print "writing...")
	;;(each [linum line (ipairs file.lines)] ;;  (file-out:write line) ;;(print linum line);;)
	(file-out:write (tcat this.lines "\n"))))



(λ f.enforce [{: name : read! : write! : lines : __is-file}]
  "will fail if *input* is not a *file*, its rudamentary but it **should** work"
  {: name : read! : write! : lines : __is-file})

(lambda f.is-file [file] (. (f.enforce file) :__is-file))

(lambda file? [this]
  (let [{:__is-file is-file} this]
	is-file))

(lambda f.new [{:name name}]
  "takes a filename in and returns a *file*"
  (local file
		 {:name name 
		  :read! read!
		  :write! write!
		  :lines []
		  :file? file?
		  :__is-file true}) file)

(lambda f.dummy []
  {:name "/dev/null"
   :lines []
   :__is-file false
   :file? file?})

(lambda f.create [file]
  (lambda new-file [{: name : lines}]
	{:name name
	 :read! read!
	 :write! write!
	 :lines lines
	 :file? file?
	 :__is-file true})
  (collect [k v (pairs (new-file file)) &into file] k v)
  file)

(lambda f.init [{: name}]
  (let [file (f.new {: name})]
	(file:read!)
	(print file.name)
	file))

f
