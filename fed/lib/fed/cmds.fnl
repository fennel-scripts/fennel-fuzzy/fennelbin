#!/usr/bin/env fennel

;;(let [home (os.getenv "HOME") fennel (require :fennel)]  (set fennel.path (.. home "/botsdir/lib/" "?.fnl" ";" fennel.path)))
(local rl (require :readline))
(local gum (require :bubblegum))

(local prompts (require :fed.prompts))
(local {: line : lines} (require :fed.lines))
(local file-lib (require :fed.file))
(local plates (require :fed.plates))

(local fringe (require :fed.fringe))
(local default-fringe (fringe.new {:a " " :b " " :c " " :d " " :linum " "}))
(local line-fringe (fringe.new {:a "-" :b "-" :c "-" :d "-" :linum "-"}))
(local tcat table.concat)


(lambda capture [cmd]
  ;;(print cmd)
  (local f (assert (io.popen cmd :r)))
  (var s (assert (f:read :*a)))
  (local x [(f:close)])
  (local raw s)
  (set s (string.gsub s "^%s+" ""))
  (set s (string.gsub s "%s+$" ""))
  (set s (string.gsub s "[\n\r]+" " "))
  ;;(each [key value (ipairs x)]	(print " " key value))
  {:out s
   :raw raw
   :bool (. x 3)})

 (lambda split [input file ?sep]
   (let [sep (or ?sep "%s")]
 	(let [result {}
 		  matches (string.gmatch input (.. "([^" sep "]+)"))]
 	  (each [str matches] (var strng str)
			(when (= strng "!mark!")
			  (set strng file.mark))
			(table.insert result strng))
 	  result)))

(local cmds {})


(lambda word-get [file linum]
  (let [lines file.lines
		linum (tonumber linum)]
	(split (. lines linum) file)
	))

(lambda word-list [file linum]
  (let [linum (tonumber linum)]
	(each [index word (ipairs (word-get file linum))]
	  (print index word)
	  )))
(local easy {})
(lambda easy.tostring [value]
  (match (type value)
	:table (table.concat value " ")
	_ (tostring value)
	))


(lambda word-set! [file linum index to]
  (let [linum (tonumber linum)
		index (tonumber index)
		words (word-get file linum)]
	(tset words index (easy.tostring to))
	(line.set! file linum words)))



(lambda cmds.print [cmd file]
  (match cmd
	(where (or [:line from to] [:lines from to] [:l from to])) (lines.print-range file {:from (tonumber from) :to (tonumber to)})
	[:line linum] (line.print file linum)
	[:words linum] (word-list file linum)
	(where (or [:lines] [:l])) (lines.print file)
	[:sep] (fringe.print line-fringe "###############################################")
	))

;;(gum.input {:placeholder "name" :header "what is your name"})

(lambda gum-read-line [file linenum]
  (let [lines file.lines
        linum (tonumber linenum)]
    (let [line (. lines linum)]
      (gum.input
	   {:placeholder (or line :empty)
        :head "new content"
        :prompt prompts.read-line
        :val (or line "")}
	   ))))

(lambda gum-read-word [file linenum index]
  (let [words (word-get file linenum)]
	(gum.input
	 {:placeholder (or (. words index) "")
      :head "new content"
      :prompt prompts.read-line
      :val (or (. words index) "")}
	 )))



(lambda cmds.edit [cmd file]
  (case cmd
	;;(where (or [:line linum] [:l linum])) (let [linum (tonumber linum)] (line.print file linum) (let [newcontent (rl.readline (prompts.read-line))](line.set! file linum newcontent)))
	[:line linum & to] (line.set! file linum to)
	[:l linum & to]	(line.set! file linum to)
	[:line linum] (line.set! file linum (gum-read-line file linum))
	[:l linum] (line.set! file linum (gum-read-line file linum))
	[:s linum ptrn replacement] (line.set! file linum (line.filter file linum ptrn replacement))
	[:sub linum ptrn replacement] (line.set! file linum (line.filter file linum ptrn replacement))
	[:word linum index & to] (word-set! file linum index to)
	[:word linum index] (word-set! file linum index (gum-read-word file line index))
	))

(lambda cmds.mark [cmd file]
	 (print file.mark)
  (case cmd
	[:set in] (set file.mark in)
	)
  )




(lambda find-line [file pattern]
  (let [lines file.lines
        ptrn pattern]
    (var matches [])
    (each [linenum text (ipairs lines)]
      (let [matched (string.match text ptrn)]
        (when matched
          (table.insert matches {: linenum :text matched :line text}))))
    ;;(print (.. "! " linenum) )
    (let [frin (fringe.new default-fringe)]
      (set frin.c "*")
      (each [k v (ipairs matches)]
        (set frin.linum v.linenum)
        ;;(fringe.print frin v.text)
        (fringe.print frin (or v.line :error))))))






(lambda cmds.find [cmd file]
  (match cmd
    (where (or [:line ptrn] [:l ptrn])) (find-line file ptrn)))

(lambda get-file-name []
  (or (. (capture "xplr --read-only --vroot .") :out) (gum.file {}) "project.fnl"))

(lambda line-insert [file text ?index]
  (let [lines file.lines
		index (or (tonumber ?index) 1)
		text (table.concat text)]
	(table.insert lines index text)))

(lambda cmds.eval [cmd file stack]
  ;;(var file file-in)
  (var continue true)
  (match cmd
	[:open filename] (do (set file.name filename)  (file-lib.create file))
	[:open]	(do (set file.name (get-file-name)) (file-lib.create file))
	[:read] (when (file-lib.is-file file) (file:read!))
	[:print & args] (cmds.print args file)
	[:edit & args] (cmds.edit args file)
	;;[:insert & args]
	[:insert :line index & text] (line-insert file text index)
	[:insert & text] (line-insert file text)
	;;[:substitute & args] (do (print "substitute" (tcat args)) nil)
	;;[:subst & args]	(do (print "substitute" (tcat args)) nil)
	[:find & args] (cmds.find args file)
	(where (or [:wq] [:sq] [:write :quit] [:save :quit]))
	(do (file:write!)
		(rl.save_history)
		;;(os.exit)
		(set continue false)
		)
	(where (or [:q]	[:quit] [:x] [:exit] [:close]))
	(do (rl.save_history) (os.exit))
	(where (or [:w] [:s] [:save] [:write]))
	(file:write!)
	[:wait :then & newcmd] (do (io.read) (print newcmd) (cmds.eval newcmd file stack))
	)
  continue
  )



cmds


