#!/usr/bin/env fennel

(local prompts {})
(local tcat table.concat)
(lambda prompts.default [_] "the default prompt" "λ: ")
(lambda prompts.file-info [file]
  (let [sections (string.gmatch file.name "[%w%.]+")]
	(var section "")
	(each [value sections] (set section value))
	(tcat [section (.. "[" (length file.lines) "]") " λ: "])))

(lambda prompts.read-file-name [_] (tcat ["open a file" " λ: "]))
(lambda prompts.read-line [_] (tcat ["[insert]" " λ: "]))

prompts
