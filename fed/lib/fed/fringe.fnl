#!/usr/bin/env fennel


(local fringe {})

(lambda fringe.print [f text] (print (table.concat [f.a f.b f.c f.d f.linum]) (.. "| " text)))

(fn fringe.new [{: a : b : c : d : linum}]
  {:a (or a " ")
   :b (or b " ")
   :c (or c " ")
   :d (or d " ")
   :linum (or linum " ")})



(fn fringe.test []
  (local f (fringe.new))
  (set f.a "!")
  (set f.linum 1)
  (fringe.print f "hello world")
  (set f.linum 2)
  (fringe.print f "hello world")
  (set f.linum 3)
  (fringe.print f "hello world")
  (set f.d ">")
  (set f.linum 4)
  (fringe.print f "hello world")
  (set f.linum 10)
  (fringe.print f "hello world")
  (io.read)
  )

fringe
