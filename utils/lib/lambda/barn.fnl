#!/usr/bin/env fennel
(local utils (require :lib.lambda.utils))


(local barn
	   {:desc "a simple library to do semi-oop in fennel and lua"})


(lambda barn.new [this name props]
  (let [o props]
	(set o.name name)
	(setmetatable o this)
	(set this.__index this)
	o
	))


barn
